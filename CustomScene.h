#ifndef CUSTOM_SCENE_H
#define CUSTOM_SCENE_H

#include "Scene.h"

class CustomScene : public Scene
{
public:
	using Scene::Render;
	using Scene::initializeSceneFromScript;
	
	CustomScene(){}
	~CustomScene(){}
	
	////initializing scenes
	////assignment 4: basic geometry, shading, texturing, and transparency
	void initializeCustomBasicScene4();
	
	////assignment 5: acceleration structures
	void initializeCustomAccelerationStructureG5();
	void initializeCustomAccStruct2();
	void initializeSceneAccelerationStructureBVH5();

	
	////assignment 6: partipating media and camera effects
	//Incomplete
    void initializeRayTracerImage();
    //	void initializeRayTracerImageOrig();
    
    
    ////void custom functions
    void addChainLink(const STPoint3& middle_top, const STVector3& lat_dir, const STVector3& long_dir, const float radius=1.f);
    
	////void commonly used functions -- Taken from Example Scene
	void addGround(const STPoint3& min_corner,const STVector2& size,bool counterclockwise=true);
	void addBackgroundWall(const STPoint3& min_corner,const STVector2& size,bool counterclockwise=true);
	void addWall(const STPoint3& min_corner,const STVector3& u,const STVector3& v,bool counterclockwise=true);
};

#endif

