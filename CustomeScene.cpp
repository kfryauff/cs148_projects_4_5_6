/* CS148 Fall 2015
 * K. Fryauff & M. Fonua
 */

#include "CustomScene.h"

/* **** Assignment 4 **** */
void CustomScene::initializeCustomBasicScene4(){
    rtClear();
    
    //Global Settings
    rtCamera(/*eye*/STPoint3(0.f,10.f,15.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,0.f,0.f),/*fov*/45.f,/*aspect*/1.f);
    rtOutput(/*width*/512,/*height*/512,/*path*/"../Assign4_Products/AccelTest2.png");
    rtBounceDepth(5);
    rtUseTransparentShadow(true);
    //rtUseShadow(false);	//default true
    rtShadowBias(.001f);
    //	rtSampleRate(4);
    
    
    //Lighting
    rtAmbientLight(STColor3f(.1f,.1f,.1f));
    rtPointLight(STPoint3(-2.f,5.f,5.f),STColor3f(0.3f, .3f,0.5f));
    rtPointLight(STPoint3(-5.f,5.f,2.f),STColor3f(0.6f,0.6f,.6f));
    //Trying a light inside of the sphere
    rtPointLight(STPoint3(0.f,3.f,2.f),STColor3f(.5f,.2f,.2f));
    
    //Scene Objects
    
    //Define Materials
    Material mat_box(/*ambient*/STColor3f(1.f,0.f,0.6f),/*diffuse*/STColor3f(0.8f,0.f,.2f),/*specular*/STColor3f(0.f,0.f,0.f),/*mirror*/STColor3f(0.f,0.f,0.f),/*shiness*/50.f);
    Material mat_sphere(STColor3f(0.f,0.f,1.f),STColor3f(0.8f,0.f,0.f),STColor3f(1.f,1.f,1.f),STColor3f(0.f,0.f,0.f),30.f);
    Material mat_glass1(/*ambient*/STColor3f(),/*diffuse*/STColor3f(),/*spec*/STColor3f(0.f,0.f,0.f),/*mirror*/STColor3f(0.f,0.f,0.f),/*shiness*/0.f,/*refr*/STColor3f(.9f,.3f,.1f),/*sn*/1.3f);
    Material mat_glass2(/*ambient*/STColor3f(),/*diffuse*/STColor3f(),/*spec*/STColor3f(1.f,1.f,1.f),/*mirror*/STColor3f(.1f,.1f,.1f),/*shiness*/30.f,/*refr*/STColor3f(.7f,.6f,.9f),/*sn*/1.3f);
    Material mat_metal(/*ambient*/STColor3f(.6f,.4f,.3f),/*diffuse*/STColor3f(.6f,.4f,.3f),/*spec*/STColor3f(.2f,.2f,.2f),/*mirror*/STColor3f(.6f,.4f,.3f),/*shiness*/90.f);
    
    //Draw Box
    rtMaterial(mat_metal);
    rtBox(/*center*/STPoint3(0.f,1.f,2.f),/*size*/STVector3(4.f,2.f,4.f));
    
    //Draw Sphere
    //rtMaterial(mat_sphere);
    rtMaterial(mat_glass2);
    rtSphere(STPoint3(0.f,3.f,2.f),1.f);
    
    //Draw Environment
    
    //Define Materials
    Material mat_ground(STColor3f(.4f,.4f,.4f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),30.f);
    Material mat_wall(STColor3f(1.f,1.f,1.f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),0.f);
    
    rtMaterial(mat_ground);
    //Draw Ground
    addGround(STPoint3(-8.f,0.f,-5.f),STVector2(16.f,16.f));
    //Draw Ceiling
    addGround(STPoint3(-8.f,16.f,-5.f),STVector2(16.f,16.f),false);
    //Draw Background Wall
    addBackgroundWall(STPoint3(-8.f,0.f,-5.f),STVector2(16.f,16.f));
    //Draw Forward Wall
    addBackgroundWall(STPoint3(-8.f,0.f,16.f),STVector2(16.f,16.f),false);
    //Draw Left Wall
    addWall(STPoint3(-8.f,0.f,-5.f),STVector3(0.f,16.f,0.f),STVector3(0.f,0.f,16.f),true);
    //Draw Right Wall
    addWall(STPoint3(8.f,0.f,-5.f),STVector3(0.f,16.f,0.f),STVector3(0.f,0.f,16.f),false);
    
    
    //Acceleration Structure
    ////aabb tree
    accel_structure=AABB_TREE;
    AABBTree* aabb_tree=new AABBTree(objects);
    aabb_trees.push_back(aabb_tree);
    
    //	//////uniform grid
    //	accel_structure=UNIFORM_GRID;
    //	AABB scene_bounding_box;getObjectsAABB(objects,scene_bounding_box);
    //	int subdivision[3]={20,20,2};
    //	uniform_grid=new UniformGrid(objects,scene_bounding_box,subdivision);
}

/* **** Assignment 5 **** */
void CustomScene::initializeCustomAccelerationStructureG5(){
    rtClear();
    
    ////global settings
    rtCamera(/*eye*/STPoint3(0.f,10.f,40.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,0.f,0.f),/*fov*/60.f,/*aspect*/1.f);
    rtOutput(/*width*/512,/*height*/512,/*path*/"../Assign5_Products/Test1_AccelStructGrid.png");
    rtBounceDepth(1);
    //	rtUseShadow(false);
    rtShadowBias(1e-4f);
    //	rtSampleRate(4);
    
    ////lighting
    rtAmbientLight(STColor3f(.1f,.1f,.1f));
    rtPointLight(STPoint3(0.f,0.f,10.f),STColor3f(1.f,1.f,1.f));
    //	rtPointLight(STPoint3(-5.f,0.f,10.f),STColor3f(0.3f, .3f,0.5f));
    //	rtPointLight(STPoint3(5.f,0.f,10.f),STColor3f(0.6f,0.6f,.6f));
    rtAreaLight(/*v1*/STPoint3(1.5f,.25f,.75f),/*v2*/STPoint3(1.5f,-.25f,1.25f),/*v3*/STPoint3(1.5f,.25f,1.25f),STColor3f(.8f,.8f,.8f));
    
    ////objects
    int counts[3]={10,10,1};
    float size[3]={40.f,40.f,4.f};
    STPoint3 min_corner(-size[0]*.5f,-size[1]*.5f,-size[2]*.5f);
    STPoint3 max_corner(size[0]*.5f,size[1]*.5f,size[2]*.5f);
    STVector3 dx(size[0]/(float)counts[0],size[1]/(float)counts[1],size[2]/(float)counts[2]);
    
    ////spheres,cylinders and cubes
    for(int i=0;i<counts[0];i++){
        for(int j=0;j<counts[1];j++){
            for(int k=0;k<counts[2];k++){
                STColor3f color((float)rand()/RAND_MAX,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX);
                Material mat(color,color,STColor3f(),STColor3f(0,0,0),40.f);
                STVector3 perturb((float)rand()/RAND_MAX,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX);
                STPoint3 center=min_corner+STVector3(dx.Component(0)*(float)(i+.5f+(perturb.Component(0)-.5f)*.2f),dx.Component(1)*(float)(j+.5f+(perturb.Component(1)-.5f)*.2f),dx.Component(2)*(float)(k+.5f+(perturb.Component(2)-.5f)*.2f));
                rtMaterial(mat);
                
                rtPushMatrix();
                rtTranslate(center.x, center.y, center.z);
                int mode=rand()%3;
                float rz = rand()%360;
                float rx = -45+ (rand()%90);
                float ry = -45+ (rand()%90);
                rtRotate(rx, ry, rz);
                switch(mode){
                    case 0:rtSphere(STPoint3(0,0,0),.75f);break;
                    case 1:rtBox(STPoint3(0,0,0),STVector3(1.5f));break;
                    case 2:rtCylinder(STPoint3(0,0,0), STPoint3(0,0,.75*(1+rand()%3)), 0.5+((float)rand()/RAND_MAX)*0.5);
                }
                rtPopMatrix();
            }
        }
    }
    
    //background
    STColor3f background_color(.0f,.2f,.8f);
    Material background_mat(background_color,background_color,STColor3f(.2f,.2f,.2f),STColor3f(),40.f);
    rtMaterial(background_mat);
    addBackgroundWall(/*min_corner*/STPoint3(-size[0]*.6f,-size[1]*.6f,-2.f),/*size*/STVector2(size[0]*1.2f,size[1]*1.2f));
    
    //use acceleration structure
    ////aabb tree
    accel_structure=AABB_TREE;
    AABBTree* aabb_tree=new AABBTree(objects);
    aabb_trees.push_back(aabb_tree);
    
    //	//////uniform grid
    //	accel_structure=UNIFORM_GRID;
    //	AABB scene_bounding_box;getObjectsAABB(objects,scene_bounding_box);
    //	int subdivision[3]={20,20,2};
    //	uniform_grid=new UniformGrid(objects,scene_bounding_box,subdivision);
}

void CustomScene::initializeCustomAccStruct2(){
    rtClear();
    
    ////global settings
    rtCamera(/*eye*/STPoint3(0.f,0.f,40.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,0.f,0.f),/*fov*/60.f,/*aspect*/1.f);
    rtOutput(/*width*/512,/*height*/512,/*path*/"../Assign5_Products/Test2_AccelStructGrid.png");
    rtBounceDepth(1);
    //	rtUseShadow(false);
    rtShadowBias(1e-4f);
    //	rtSampleRate(4);
    
    ////lighting
    rtAmbientLight(STColor3f(.1f,.1f,.1f));
    rtPointLight(STPoint3(0.f,0.f,10.f),STColor3f(1.f,1.f,1.f));
    
    ////objects
    int counts[3]={20,20,1};
    float size[3]={40.f,40.f,4.f};
    STPoint3 min_corner(-size[0]*.5f,-size[1]*.5f,-size[2]*.5f);
    STPoint3 max_corner(size[0]*.5f,size[1]*.5f,size[2]*.5f);
    STVector3 dx(size[0]/(float)counts[0],size[1]/(float)counts[1],size[2]/(float)counts[2]);
    
    ////spheres,cylinders and cubes
    for(int i=0;i<counts[0];i++){
        for(int j=0;j<counts[1];j++){
            for(int k=0;k<counts[2];k++){
                float concentration = (float)rand()/RAND_MAX;
                STColor3f color(concentration,.5f, .5f);
                Material mat(color,color,STColor3f(),STColor3f(0,0,0),40.f);
                STVector3 perturb((float)rand()/RAND_MAX,(float)rand()/RAND_MAX,(float)rand()/RAND_MAX);
                STPoint3 center=min_corner+STVector3(dx.Component(0)*(float)(i+.5f+(perturb.Component(0)-.5f)*.2f),dx.Component(1)*(float)(j+.5f+(perturb.Component(1)-.5f)*.2f),dx.Component(2)*(float)(k+.5f+(perturb.Component(2)-.5f)*.2f));
                rtMaterial(mat);
                
                rtPushMatrix();
                
                rtTranslate(center.x, center.y, center.z);
                float rz = concentration*360.0;
                rtRotate(0.f, 0.f, rz);
                rtBox(STPoint3(0,0,0),STVector3(1.5f));
                
                rtPopMatrix();
            }
        }
    }
    
    //background
    STColor3f background_color(.0f,.2f,.8f);
    Material background_mat(background_color,background_color,STColor3f(.2f,.2f,.2f),STColor3f(),40.f);
    rtMaterial(background_mat);
    addBackgroundWall(/*min_corner*/STPoint3(-size[0]*.6f,-size[1]*.6f,-2.f),/*size*/STVector2(size[0]*1.2f,size[1]*1.2f));
    
    //use acceleration structure
    ////aabb tree
    accel_structure=AABB_TREE;
    AABBTree* aabb_tree=new AABBTree(objects);
    aabb_trees.push_back(aabb_tree);
    
    //	//////uniform grid
    //	accel_structure=UNIFORM_GRID;
    //	AABB scene_bounding_box;getObjectsAABB(objects,scene_bounding_box);
    //	int subdivision[3]={20,20,2};
    //	uniform_grid=new UniformGrid(objects,scene_bounding_box,subdivision);
}

void CustomScene::initializeSceneAccelerationStructureBVH5()
{
    
    
    rtClear();
    
    //Global Settings
    rtCamera(/*eye*/STPoint3(0.f,10.f,15.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,0.f,0.f),/*fov*/45.f,/*aspect*/1.f);
    rtOutput(/*width*/512,/*height*/512,/*path*/"../Assign5_Products/AccelTest.png");
    rtBounceDepth(5);
    rtUseTransparentShadow(true);
    //rtUseShadow(false);	//default true
    rtShadowBias(.001f);
    //	rtSampleRate(4);
    
    
    //Lighting
    rtAmbientLight(STColor3f(.1f,.1f,.1f));
    rtPointLight(STPoint3(-2.f,5.f,5.f),STColor3f(0.3f, .3f,0.5f));
    rtPointLight(STPoint3(-5.f,5.f,2.f),STColor3f(0.6f,0.6f,.6f));
    //Trying a light inside of the sphere
    rtPointLight(STPoint3(0.f,3.f,2.f),STColor3f(.5f,.2f,.2f));
    
    //Scene Objects
    
    //Define Materials
    Material mat_box(/*ambient*/STColor3f(1.f,0.f,0.6f),/*diffuse*/STColor3f(0.8f,0.f,.2f),/*specular*/STColor3f(0.f,0.f,0.f),/*mirror*/STColor3f(0.f,0.f,0.f),/*shiness*/50.f);
    Material mat_sphere(STColor3f(0.f,0.f,1.f),STColor3f(0.8f,0.f,0.f),STColor3f(1.f,1.f,1.f),STColor3f(0.f,0.f,0.f),30.f);
    Material mat_glass1(/*ambient*/STColor3f(),/*diffuse*/STColor3f(),/*spec*/STColor3f(0.f,0.f,0.f),/*mirror*/STColor3f(0.f,0.f,0.f),/*shiness*/0.f,/*refr*/STColor3f(.9f,.3f,.1f),/*sn*/1.3f);
    Material mat_glass2(/*ambient*/STColor3f(),/*diffuse*/STColor3f(),/*spec*/STColor3f(1.f,1.f,1.f),/*mirror*/STColor3f(.1f,.1f,.1f),/*shiness*/30.f,/*refr*/STColor3f(.7f,.6f,.9f),/*sn*/1.3f);
    Material mat_metal(/*ambient*/STColor3f(.6f,.4f,.3f),/*diffuse*/STColor3f(.6f,.4f,.3f),/*spec*/STColor3f(.2f,.2f,.2f),/*mirror*/STColor3f(.6f,.4f,.3f),/*shiness*/90.f);
    
    //Draw Box
    rtMaterial(mat_metal);
    rtBox(/*center*/STPoint3(0.f,1.f,2.f),/*size*/STVector3(4.f,2.f,4.f));
    
    //Draw Sphere
    //rtMaterial(mat_sphere);
    rtMaterial(mat_glass2);
    rtSphere(STPoint3(0.f,3.f,2.f),1.f);
    
    //Draw Environment
    
    //Define Materials
    Material mat_ground(STColor3f(.4f,.4f,.4f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),30.f);
    Material mat_wall(STColor3f(1.f,1.f,1.f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),0.f);
    
    rtMaterial(mat_ground);
    //Draw Ground
    addGround(STPoint3(-8.f,0.f,-5.f),STVector2(16.f,16.f));
    //Draw Ceiling
    addGround(STPoint3(-8.f,16.f,-5.f),STVector2(16.f,16.f),false);
    //Draw Background Wall
    addBackgroundWall(STPoint3(-8.f,0.f,-5.f),STVector2(16.f,16.f));
    //Draw Forward Wall
    addBackgroundWall(STPoint3(-8.f,0.f,16.f),STVector2(16.f,16.f),false);
    //Draw Left Wall
    addWall(STPoint3(-8.f,0.f,-5.f),STVector3(0.f,16.f,0.f),STVector3(0.f,0.f,16.f),true);
    //Draw Right Wall
    addWall(STPoint3(8.f,0.f,-5.f),STVector3(0.f,16.f,0.f),STVector3(0.f,0.f,16.f),false);
    
    
    //Acceleration Structure
    ////aabb tree
    accel_structure=AABB_TREE;
    AABBTree* aabb_tree=new AABBTree(objects);
    aabb_trees.push_back(aabb_tree);
    
    //	//////uniform grid
    //	accel_structure=UNIFORM_GRID;
    //	AABB scene_bounding_box;getObjectsAABB(objects,scene_bounding_box);
    //	int subdivision[3]={20,20,2};
    //	uniform_grid=new UniformGrid(objects,scene_bounding_box,subdivision);
}

/* **** Raytracer **** */
void CustomScene::initializeRayTracerImage(){
    
    rtClear();
    
    /* ** Global Settings ** */
    
    //	rtCamera(/*eye*/STPoint3(-.5f,1.9f,4.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,2.5f,1.f),/*fov*/45.f,/*aspect*/1.f);
    //Grid View
    //	rtCamera(/*eye*/STPoint3(0.f,30.f,60.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,0.f,0.f),/*fov*/45.f,/*aspect*/1.f);
    //Image View
    
    //  zoomed in view
    //	rtCamera(/*eye*/STPoint3(-15.f,40.f,15.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,20.f,0.f),/*fov*/45.f,/*aspect*/1.f);
    
    //  zoomed out view
    rtCamera(/*eye*/STPoint3(-35.f,40.f,35.f),/*up*/STVector3(0.f,1.f,0.f),/*lookat*/STPoint3(0.f,20.f,0.f),/*fov*/45.f,/*aspect*/1.f);
    
    rtOutput(/*width*/1200,/*height*/800,/*path*/"../Assign6_Products/RayTrace_Test2.png");
    rtBounceDepth(5);
    rtUseTransparentShadow(true);
    //rtUseShadow(false);	//default true
    rtShadowBias(1e-4f);
    rtSampleRate(4);
    
    /* ** Lighting ** */
    
    rtAmbientLight(STColor3f(.1f,.1f,.1f));
    rtPointLight(STPoint3(-5.f,30.f,3.f), STColor3f(0.3f, 0.3f,0.5f));
    rtPointLight(STPoint3(-3.f,30.f,5.f), STColor3f(0.3f, 0.3f,0.5f));
    //Trying a light inside of the sphere
    //	rtPointLight(STPoint3(0.f,3.f,2.f),STColor3f(.5f,.2f,.2f));
    
    /* ** Scene Objects ** */
    
    // Define Materials
    // Material var(ambient, diffuse, specular, mirror, shiness, [refr, sn]
    Material mat_box(
                     /*ambient*/    STColor3f(1.f,0.f,0.6f),
                     /*diffuse*/    STColor3f(0.8f,0.f,.2f),
                     /*specular*/   STColor3f(0.f,0.f,0.f),
                     /*mirror*/     STColor3f(0.f,0.f,0.f),
                     /*shiness*/    50.f
                     );
    Material mat_sphere(
                        /*ambient*/     STColor3f(0.f,0.f,1.f),
                        /*diffuse*/     STColor3f(0.8f,0.f,0.f),
                        /*specular*/    STColor3f(1.f,1.f,1.f),
                        /*mirror*/      STColor3f(0.f,0.f,0.f),
                        /*shiness*/     30.f
                        );
    Material mat_glass_lens(
                            /*ambient*/     STColor3f(),
                            /*diffuse*/     STColor3f(),
                            /*spec*/        STColor3f(1.f,1.f,1.f),
                            /*mirror*/      STColor3f(0.f,0.f,0.f),
                            /*shiness*/     0.f,
                            /*refr*/        STColor3f(1.f,1.f,1.f),
                            /*sn*/          .5f
                            );
    Material mat_glass1(
                        /*ambient*/     STColor3f(),
                        /*diffuse*/     STColor3f(),
                        /*spec*/        STColor3f(0.f,0.f,0.f),
                        /*mirror*/      STColor3f(0.f,0.f,0.f),
                        /*shiness*/     0.f,
                        /*refr*/        STColor3f(.9f,.3f,.5f),
                        /*sn*/          1.3f
                        );
    Material mat_glass2(
                        /*ambient*/     STColor3f(),
                        /*diffuse*/     STColor3f(),
                        /*spec*/        STColor3f(1.f,1.f,1.f),
                        /*mirror*/      STColor3f(.1f,.1f,.1f),
                        /*shiness*/     30.f,
                        /*refr*/        STColor3f(.7f,.6f,.9f),
                        /*sn*/          1.3f
                        );
    
    Material mat_metal(
                       /*ambient*/     STColor3f(.6f,.4f,.3f),
                       /*diffuse*/     STColor3f(.6f,.4f,.3f),
                       /*spec*/        STColor3f(.2f,.2f,.2f),
                       /*mirror*/      STColor3f(.6f,.4f,.3f),
                       /*shiness*/     45.f
                       );
    Material mat_wine(
                       /*ambient*/     STColor3f(100.f, 0.f, 0.f),
                       /*diffuse*/     STColor3f(100.f, 0.f, 0.f),
                       /*spec*/        STColor3f(100.f,1.f,1.f),
                       /*mirror*/      STColor3f(.1f,.1f,.1f),
                       /*shiness*/     5.f,
                       /*refr*/        STColor3f(.7f,.6f,.9f),
                       /*sn*/          1.3f
    );
    
    
    // Add Desk
        rtPushMatrix();
        rtTranslate(0.f, 0.f, -5.f);
        rtScale(.15f, .15f, .15f);
        rtRotate(0.f, -25.f, 0.f);
        rtTranslate(-25.f, -10.f, 115.f);
        rtTriangleMeshWithMaterialAndTexture("../Object_Files/office_table_lord.obj", true, true);
        rtPopMatrix();
    
//    Material mat_tri(/*ambient*/STColor3f(1.f,1.f,1.f),/*diffuse*/STColor3f(1.f,1.f,1.f),/*specular*/STColor3f(.2f,.2f,.2f),/*mirror*/STColor3f(0.f,0.f,0.f),/*shiness*/40.f);
//    rtMaterial(mat_tri);
//    addGround(STPoint3(11.f,21.7f,-9.f),STVector2(-20.f,20.f), false);
    
    
    // Reorient to desk height and room corner
    rtPushMatrix();
    rtTranslate(0.f, 0.f, -5.f);
    rtTranslate(0.f, 21.7f, 0.f);
    
    // Add Desk Environment
    
    // Add Desk Scene
    
    
    // Add Glass
    rtPushMatrix();
    rtTranslate(-5.f, 0.f, 5.f);
    rtScale(1.f, 1.2f, 1.f);
    rtScale(2.f, 2.f, 2.f);
    rtMaterial(mat_glass1);
    rtAttenuation(1.f);
    rtTriangleMesh("../Object_Files/glass/wineglass_standalone.obj", true, false);
    //    rtTranslate(0.5f, 0.f, -0.5f);
    rtMaterial(mat_wine);
    rtTriangleMesh("../Object_Files/glass/winecontent_top.obj", true, true);
    rtPopMatrix();
    
    //    Draw Glasses
    
    
    //    Add Glasses
    rtPushMatrix();
    rtMaterial(mat_glass_lens);
    rtAttenuation(1.f);
    rtTranslate(-5.f, 0.f, 5.f);
    rtRotate(0.f, 180.f, 0.f);
    rtScale(.15f, .2f, .15f);
    rtScale(2.f, 2.f, 2.f);
    rtTranslate(0.f, -2.f, 0.f);
    //   	rtSphere(STPoint3(0.f,3.f,-2.f),.5f);
    rtTriangleMesh("../Object_Files/spectacles_lens.obj",true,false);
    rtTriangleMeshWithMaterialAndTexture("../Object_Files/spectacles.obj",true,false);
    //rtSphere(STPoint3(0.f,3.f,2.f),1.f);
    rtPopMatrix();
    
    addChainLink(STPoint3(0.f, 1.f, 1.f),STVector3(0.f, 10.f, 0.f), STVector3(0.f, 0.f, 20.f), 10.f);
    
    // Pop Matrix for reorienting to desk height and room corner
    rtPopMatrix();
    
    
    
    //Load Textures
    //	int box_tex_id;
    //	rtLoadTexture("../Texture_Files/displacementmap.jpeg",box_tex_id);
    
    //	//Draw Box
    //	rtMaterial(mat_metal);
    //	rtBindTexture(box_tex_id);
    //	rtBox(/*center*/STPoint3(0.f,.5f,2.f),/*size*/STVector3(4.f,1.9f,4.f));
    //	rtBox(STPoint3(0.f, 1.75f, 2.f), STVector3(3.5f, .5f, 3.5f));
    //	rtUnbindTexture();
    //
    //	//Draw Sphere
    //	rtMaterial(mat_sphere);
    //	rtSphere(STPoint3(0.f,3.f,-2.f),1.f);
    
    
    
    
    
    //Add Headphones
    //	rtPushMatrix();
    //	rtTranslate(0.f, 17.f, 0.f);
    //	rtScale(.01f, .01f, .01f);
    //	rtTranslate(-15.f, 0.f, 15.f);
    //	rtTriangleMeshWithMaterialAndTexture("../Object_Files/Sony.obj", true, false);
    //	rtPopMatrix();
    
    //Add lamp
    //	rtPushMatrix();
    //	rtTranslate(-10.f, 15.5f, 0.f);
    //	//Light Source
    //	rtPushMatrix();
    //	rtScale(2.f, 2.f, 2.f);
    //	rtPointLight(STPoint3(.5f,1.25f,-0.5f),STColor3f(1.f,1.f,1.f));
    //	rtPointLight(STPoint3(-.5f,1.25f,-0.5f),STColor3f(1.f,1.f,1.f));
    //	rtPopMatrix();
    //
    //	rtPushMatrix();
    ////	rtScale(.05f, .05f, .05f);
    //	rtPointLight(STPoint3(0.5f, 4.f, 0.5f), STColor3f(1.f,1.f,1.f));
    //	rtPointLight(STPoint3(-1.f, 4.f, 0.5f), STColor3f(1.f,1.f,1.f));
    //	rtPopMatrix();
    //	//Lamp B
    //	rtPushMatrix();
    //	rtScale(.01f, .01f, .01f);
    //	rtTranslate(0.f, 0.f, 0.f);
    //	rtRotate(-90.f, 0.f, 0.f);
    //	rtTriangleMeshWithMaterialAndTexture("../Object_Files/tableLamp/lmp041.obj", true, true);
    //	rtPopMatrix();
    //
    //	rtPopMatrix();
    
    //Add globe
    //	rtPushMatrix();
    //	rtTranslate(7.f, 22.f, 5.f);
    //	rtScale(.01f, .01f, .01f);
    //	rtTranslate(0.f, -25.f, 0.f);
    //	//rtRotate(-90.f, 0.f, 0.f);
    ////	rtTriangleMesh("../Object_Files/globe/globe.obj", true, true);
    //	rtTriangleMeshWithMaterialAndTexture("../Object_Files/globe/globe.obj", true, true);
    //	rtPopMatrix();
    
    //Add Chair
    //	rtPushMatrix();
    //	rtTranslate(0.f, 0.f, 10.f);
    //	rtScale(.5f, .5f, .5f);
    //	//rtTranslate(40.f, 0.f, 15.f);
    //	rtRotate(0.f, 140.f, 0.f);
    //	rtTriangleMeshWithMaterialAndTexture("../Object_Files/chair/circlecutoutchair_chocolate.obj", true, true);
    //	rtPopMatrix();
    
    //Add Bookshelf
    //	rtPushMatrix();
    //	rtScale(.4f, .4f, .4f);
    //	rtTranslate(0.f, 40.f, -10.f);
    //	rtTriangleMeshWithMaterialAndTexture("../Object_Files/bookshelf_OBJ/bookshelf.obj", true, true);
    //	rtPopMatrix();
    
    //Draw Picture Frame
    //	rtPushMatrix();
    //	rtMaterial(mat_glass1);
    //	rtCylinder(STPoint3(0.f, 0.f, 0.f), STPoint3(0.f, -3.f, 0.f), 0.5f);
    //	rtCylinder(STPoint3(0.f, -3.f, 0.f), STPoint3(3.f, -3.f, 0.f), 0.5f);
    //	rtCylinder(STPoint3(3.f, -3.f, 0.f), STPoint3(3.f, 0.f, 0.f), 0.5f);
    //	rtCylinder(STPoint3(3.f, 0.f, 0.f), STPoint3(0.f, 0.f, 0.f), 0.5f);
    //	rtPopMatrix();
    
    /* ** Draw Environment ** */
    
    //Define Materials
    Material mat_ground(STColor3f(.4f,.4f,.4f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),30.f);
    Material mat_L_wall(STColor3f(.3f,.5f,.9f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),0.f);
    Material mat_R_wall(STColor3f(.1f,.6f,.7f),STColor3f(0.8f,0.8f,.8f),STColor3f(0.f,0.f,0.f),STColor3f(0.f,0.f,0.f),0.f);
    
    //Load Textures
    int bg_tex_id;
    rtLoadTexture("../Object_files/textures/carpet-texture.jpg",bg_tex_id);
    
    //Draw Ground
    rtMaterial(mat_ground);
    rtBindTexture(bg_tex_id);
    addGround(STPoint3(10.f,0.f,-10.f),STVector2(-40.f,40.f), false);
    rtUnbindTexture();
    
    //Draw Right Wall
    rtMaterial(mat_R_wall);
    addWall(STPoint3(10.f,0.f,-10.f),STVector3(0.f,50.f,0.f),STVector3(0.f,0.f,40.f),false);
    
    //Draw Left Wall
    rtMaterial(mat_L_wall);
    addWall(STPoint3(10.f,0.f,-10.f),STVector3(0.f,50.f,0.f),STVector3(-40.f,0.f,0.f));
    
    
    
    //Acceleration Structure
    ////aabb tree
    accel_structure=AABB_TREE;
    AABBTree* aabb_tree=new AABBTree(objects);
    aabb_trees.push_back(aabb_tree);
    
}


/* **** Custom Chainlink Code *** */
void CustomScene::addChainLink(const STPoint3& middle_top, const STVector3& lat_dir, const STVector3& long_dir, const float radius/*=1.f*/) {
    STPoint3 top_left_corner = middle_top-(lat_dir / 2.f);
    STPoint3 top_right_corner = middle_top+(lat_dir / 2.f);
    STPoint3 bottom_left_corner = top_left_corner + long_dir;
    STPoint3 bottom_right_corner = top_right_corner + long_dir;
    
    rtCylinder(top_left_corner, top_right_corner, radius);
    rtCylinder(top_left_corner, bottom_left_corner, radius);
    rtCylinder(top_right_corner, bottom_right_corner, radius);
    rtCylinder(bottom_right_corner, bottom_left_corner, radius);
    
}

/* **** Ground & Wall Code **** */

void CustomScene::addGround(const STPoint3& min_corner,const STVector2& size,bool counterclockwise/*=true*/)
{
    if(counterclockwise){
        rtTriangle(min_corner,STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z+size.y),STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z));
        rtTriangle(min_corner,STPoint3(min_corner.x,min_corner.y,min_corner.z+size.y),STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z+size.y));
    }
    else{
        rtTriangle(min_corner,STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z),STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z+size.y));
        rtTriangle(min_corner,STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z+size.y),STPoint3(min_corner.x,min_corner.y,min_corner.z+size.y));
    }
}

void CustomScene::addBackgroundWall(const STPoint3& min_corner,const STVector2& size,bool counterclockwise/*=true*/)
{
    if(counterclockwise){
        rtTriangle(min_corner,STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z),STPoint3(min_corner.x+size.x,min_corner.y+size.y,min_corner.z));
        rtTriangle(min_corner,STPoint3(min_corner.x+size.x,min_corner.y+size.y,min_corner.z),STPoint3(min_corner.x,min_corner.y+size.y,min_corner.z));
    }
    else{
        rtTriangle(min_corner,STPoint3(min_corner.x+size.x,min_corner.y+size.y,min_corner.z),STPoint3(min_corner.x+size.x,min_corner.y,min_corner.z));
        rtTriangle(min_corner,STPoint3(min_corner.x,min_corner.y+size.y,min_corner.z),STPoint3(min_corner.x+size.x,min_corner.y+size.y,min_corner.z));
    }
}

void CustomScene::addWall(const STPoint3& min_corner,const STVector3& u,const STVector3& v,bool counterclockwise/*=true*/)
{
    if(counterclockwise){
        rtTriangle(min_corner,min_corner+u,min_corner+u+v);
        rtTriangle(min_corner,min_corner+u+v,min_corner+v);
    }
    else{
        rtTriangle(min_corner,min_corner+u+v,min_corner+u);
        rtTriangle(min_corner,min_corner+v,min_corner+u+v);
    }
}