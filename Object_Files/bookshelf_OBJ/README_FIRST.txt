Full Bookshelf***

A simple but effective bookshelf, including books, that can be used in your contemporary architectural interior, or as a background prop anywhere you may need to fill space with realistic furniture.

The model is available as a native 3dsmax 2012 scene file (.max), and also in several scaled .obj formats, for importing easily into other applications, like Blender and Poser.

You are free to use or modify this object as you see fit, for use in your scenes as a prop, or any other type of scene where you may need a full bookshelf. This model is free to use for both commercial and non commercial renders. You may not, however, resell this object, or any derivative of it; individually or as part of a package/set.

*** OBJ users, the Poser-specific OBJ version was exported with a scale conversion that should load with the appropriate scaling into most Poser 9 and above. To ensure proper import, BE SURE to DE-select all check box options boxes in the Import Options dialogue. DESELECT the "Centered", "Place on Floor", "Percent of Standard Figure Size", etc. options in the import dialogue box after you select the poser .obj file, and then the object should load with correct scale into the scene.  In Daz|Studio, simply choose the Poser scaled .obj version, and import with Poser preset from the import dialogue box.  Blender users, obviously, choose the Blender scaled .obj.  All other software applications may be best suited to import the standard .obj file.

All textures were created by me, but most of the original reference textures originated at CGTextures.com texture resource website.

Have fun, and let me know if there are any issues:  luxxeon@zoho.com

My CgSociety Portfolio:  http://luxxeon.cgsociety.org/gallery/
My DeviantArt Page:  http://luxxeon.deviantart.com/
My 3DArtist Online Page:  http://www.3dartistonline.com/user/Luxxeon
My Renderosity Gallery:  http://www.renderosity.com/mod/gallery/browse.php?user_id=754880

Thanks,

John

