#include "st.h"
#include "stgl.h"
#include "stglut.h"
#include "ExampleScene.h"
#include "CustomScene.h"
#include <iostream>
#include <time.h>

using namespace std;

int main(int argc, const char * argv[])
{
//	ExampleScene *scene = new ExampleScene();
	CustomScene *custom_scene = new CustomScene();
	
	////or set rendering scene from code
	////example scenes for assignment 4
	//Example Scenes:
//	    scene->initializeSceneBasicGeometry();
//	    scene->initializeSceneBasicLightingAndShading();
//	    scene->initializeSceneTransform();
//	    scene->initializeSceneObjMesh();
//	    scene->initializeSceneObjMesh2();
//	    scene->initializeSceneTexture();
//	    scene->initializeSceneTransparentObject();
//	    scene->initializeSceneTransparentObject2();
	
	/* Custom Scene 4: */
//	custom_scene->initializeCustomBasicScene4();
	
	////example scenes for assignment 5
	//scene->initializeSceneAccelerationStructureGrid();
//	scene->initializeSceneAccelerationStructureBVH();
	
	/* Custom Scene 5: */
//	custom_scene->initializeCustomAccelerationStructureG5();
//	custom_scene->initializeCustomAccStruct2();
	
	
	////example scenes for assignment 6
	//scene->initializeSceneDepthOfField();
	//scene->initializeSceneParticipatingMedia();
	
	//Custom Scene:
	custom_scene->initializeRayTracerImage();
	
	////set rendering scene from script files
	//scene->initializeSceneFromScript("../Standard_Tests/RecursiveTest.txt");
	//scene->initializeSceneFromScript("../Standard_Tests/CornellBox.txt");
	//scene->initializeSceneFromScript("../Standard_Tests/DoF.txt");
	//scene->initializeSceneFromScript("../Standard_Tests/Glass.txt");
	//scene->initializeSceneFromScript("../Standard_Tests/Go.txt");
	//scene->initializeSceneFromScript("../Standard_Tests/Bowl.txt");
	//scene->rtSampleRate(4);
	
	////initialize acceleration structures
	//scene->buildAccelStructures(std::string("aabb"));
	//scene->buildAccelStructures(std::string("grid"));
	
	clock_t start, end;
	start=clock();
	
//	    scene->Render();
	custom_scene->Render();
	
	end=clock();
	cout << "Render time: "<<(double)(end-start) / ((double)CLOCKS_PER_SEC)<<" s"<<std::endl;
	
	//system("PAUSE");
	
	return 0;
}

